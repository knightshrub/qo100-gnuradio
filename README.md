# QO-100 SDR components

This repository contains gnuradio components for QO-100 amateur radio satellite reception

Components so far:

- ssbmod: an SSB modulator
- ssbdemod: an SSB demodulator
- modemod: a test setup combining SSB modulation & demodulation
- ssbtest: use a PlutoSDR to transmit SSB modulated voice data
